#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { TypescriptReservationCheckLambdaStack } from '../lib/typescript-reservation-check-lambda-stack';

const app = new cdk.App();
new TypescriptReservationCheckLambdaStack(app, 'ServerlessApp', {});
