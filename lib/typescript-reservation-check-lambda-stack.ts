import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';

export class TypescriptReservationCheckLambdaStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // defines an AWS Lambda resource
    const hello_lambda = new lambda.Function(
      this,
      'HelloLambda',
      {
        runtime: lambda.Runtime.NODEJS_14_X,
        code: lambda.Code.fromAsset('./lambdas/reservation-checking-lambda'),
        handler: 'lambda.handler'
      }
    );
  }
}
